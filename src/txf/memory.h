/**
 * @file
 * @brief Memory management helpers.
 */

#pragma once

#include "txf/types.h"

/**
 * @brief Allocate a range of memory.
 *
 * Trying to allocate 0 bytes will result in a NULL pointer being returned.
 * @param length Length of the memory region to allocate.
 * @param clear Whether to clear the memory region.
 * @return Pointer to the memory region.
 */
void *alloc(UPTR length, BOOL clear);

/**
 * @brief Deallocate a range of memory.
 *
 * This function will not attempt to free a NULL pointer.
 * @param ptr Pointer to the memory region.
 */
void dealloc(void *ptr);
