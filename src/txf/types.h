/**
 * @file
 * @brief Various typedefs.
 */

#pragma once

#include <stdint.h>
#include <stdlib.h>

#undef NULL
#undef FALSE
#undef TRUE

/// @brief Null pointer.
#define NULL ((void*)0)

/// @brief 8-bit unsigned integer.
typedef uint8_t U8;
/// @brief 16-bit unsigned integer.
typedef uint16_t U16;
/// @brief 32-bit unsigned integer.
typedef uint32_t U32;
/// @brief 64-bit unsigned integer.
typedef uint64_t U64;
/// @brief Pointer-size unsigned integer.
typedef size_t UPTR;

/// @brief 8-bit signed integer.
typedef int8_t I8;
/// @brief 16-bit signed integer.
typedef int16_t I16;
/// @brief 32-bit signed integer.
typedef int32_t I32;
/// @brief 64-bit signed integer.
typedef int64_t I64;
/// @brief Pointer-size signed integer.
typedef ssize_t IPTR;

/// @brief 32-bit floating point number.
typedef float F32;
/// @brief 64-bit floating point number.
typedef double F64;

/// @brief Boolean value.
typedef U8 BOOL;

/// @brief False boolean value.
#define FALSE (BOOL)0
/// @brief True boolean value.
#define TRUE (BOOL)1

/// @brief String with length.
typedef struct STRING {
	/// @brief Length of the string.
	UPTR length;

	/// @brief Data of the string.
	U8 *ptr;
} STRING;

/**
 * @brief Make a STRING from an immediate ASCIZ string.
 * @param str ASCIZ string.
 * @return STRING made from the input string.
 */
#define MAKE_STRING(str) (STRING){ sizeof(str) - 1, (U8*)str }

/**
 * @brief Make a STRING from an ASCIZ string.
 * @param str ASCIZ string.
 * @return STRING made from the input string.
 */
STRING string_from_asciz(const char *str);

/**
 * @brief Make an ASCIZ string from a STRING.
 * @param str STRING.
 * @return ASCIZ string made from the input string.
 */
const char *asciz_from_string(STRING str);
