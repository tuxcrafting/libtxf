/**
 * @file
 * @brief Logging functions.
 */

#pragma once

/// @brief Log level of a message.
enum log_level {
	/// @brief Debug information.
	LOG_DEBUG = 0,
	/// @brief Normal information.
	LOG_INFO,
	/// @brief Warning.
	LOG_WARNING,
	/// @brief Error.
	LOG_ERROR,
	/// @brief Critical error, initiates a soft shutdown.
	LOG_CRITICAL,
	/// @brief Fatal error, causes the program to abort immediately.
	LOG_FATAL,
};

/**
 * @brief Log a message.
 * @param level Logging level.
 * @param namespace Name of the module logging the message.
 * @param format Format string, passed to printf.
 * @param ... Parameters for the format string.
 */
void log_msg(enum log_level level, const char *namespace, const char *format, ...);
