#include "txf/types.h"

#include <string.h>

#include "txf/memory.h"

STRING string_from_asciz(const char *str) {
	return (STRING){ strlen(str), (U8*)str };
}

const char *asciz_from_string(STRING str) {
	char *s = alloc(str.length + 1, TRUE);
	memcpy(s, str.ptr, str.length);
	return (const char*)s;
}
