#include "txf/memory.h"

#include <stdlib.h>
#include <string.h>

#include "txf/logging.h"
#include "txf/types.h"

void *alloc(UPTR length, BOOL clear) {
	if (length == 0) {
		return NULL;
	}
	void *ptr = malloc(length);
	if (ptr == NULL) {
		log_msg(LOG_CRITICAL, "libtxf.memory", "Allocating %zu bytes failed.", length);
	}
	if (clear) {
		memset(ptr, 0, length);
	}
	return ptr;
}

void dealloc(void *ptr) {
	if (ptr == NULL) {
		return;
	}
	free(ptr);
}
